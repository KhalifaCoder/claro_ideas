import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";


// dinamically create pages routes
import LoginRoutes from "routes/login.jsx";

import bgImage from "assets/img/background_inicio.png";

class Pages extends Component {
  componentWillMount() {
    if (document.documentElement.className.indexOf("nav-open") !== -1) {
      document.documentElement.classList.toggle("nav-open");
    }
  }
  render() {
    return (
      <div>
        <div className="wrapper wrapper-full-page">
          <div
            className={"full-page login-page"}
            data-image={bgImage}
          >
            <div className="content pt7p">
              <Switch>
                {LoginRoutes.map((prop, key) => {
                  return (
                    <Route
                      exact 
                      path={prop.path}
                      component={prop.component}
                      key={key}
                    />
                  );
                })}
              </Switch>
            </div>
            <div
              className="full-page-background"
              style={{ backgroundImage: "url(" + bgImage + ")" }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Pages;
