import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import HeaderLinks from "../Header/HeaderLinks.jsx";
import logo from "assets/img/logoAdmin.png";

import dashboardRoutes from "routes/dashboard.jsx";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    };
  }
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }
  updateDimensions() {
    this.setState({ width: window.innerWidth });
  }
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }
  render() {
    return (
      <div
        id="sidebar"
        className="sidebar"
        data-color="ideas"
      >
        <div className="sidebar-background" />
        <div className="logo">
          <a
            href="/"
            className="simple-text logo-normal"
          >
            <div className="logo-img">
              <img src={logo} alt="logo_image" />
            </div>
          </a>
        </div>
        <div className="sidebar-wrapper">
          <div className="user">
            <div className="grit">
              <div className="photo">
                <img src="https://claroideas.wigilabs.com/Images/dist/1.jpg" alt="Avatar" />
              </div>
            </div>
            <div className="info">
              <a>
                <span>Administrador</span>
                <span>Gordon Walters</span>
                <span>CLARO COLOMBIA</span>
              </a>
            </div>
          </div>
          <ul className="nav">
            {this.state.width <= 991 ? <HeaderLinks /> : null}
            {dashboardRoutes.map((prop, key) => {
              if (!prop.redirect)
                return (
                  <li
                    className={
                      prop.upgrade
                        ? "active active-pro"
                        : this.activeRoute(prop.path)
                    }
                    key={key}
                  >
                    <NavLink
                      to={prop.path}
                      className="nav-link"
                      activeClassName="active"
                    >
                      <p>{prop.name}
                        <i className={prop.icon} /></p>

                    </NavLink>
                  </li>
                );
              return null;
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default Sidebar;
