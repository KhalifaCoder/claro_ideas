import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Thumbnail } from "react-bootstrap";
import axios from 'axios';
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from 'react-block-ui';
import FileBase64 from 'react-file-base64';
import Arrow from "assets/img/left-arrow.png";

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: [], 
        casasProgramadoras:[],
        newProgrammer:true,
        nombre: "",
        textoprevio: "",
        textoprincipal: "",
        img1: "",
        img2: "",
        id: "",
        alert: null,
        editNew: false,
        preview: false,
        file1: {},
        file2: {},
        blocking: false,
        filePre: {},
        filePri: {},
        editarImgPre: false,
        editarImgPri: false
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.togglePreview = this.togglePreview.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.updateImgPre = this.updateImgPre.bind(this);
    this.updateImgPri = this.updateImgPri.bind(this);
    this.editarPre = this.editarPre.bind(this);
    this.editarPri = this.editarPri.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
  }
  toggleBox() {
    const { newProgrammer, editNew} = this.state;
    if (editNew === true) {
      this.setState({ editNew: false });
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "" });
    }
    this.setState({ newProgrammer: !newProgrammer});
  }
  togglePreview() {
    this.setState({ preview: !this.state.preview })
  }
  editarPre() {
    this.setState({ editarImgPre: !this.state.editarImgPre })
  }
  editarPri() {
    this.setState({ editarImgPri: !this.state.editarImgPri })
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  onSubmitUpdateForm() {
    const { nombre, textoprevio, textoprincipal, idprogrammer } = this.state;
    if (nombre !== "" && textoprevio !== "" && textoprincipal !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { nombre, textoprevio, textoprincipal, idprogrammer } };
      dataSend.metodo = "/admin/houses/update";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadProgrammers();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("newsForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", textoprevio: "", textoprincipal: ""});
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  updateImg(file,tipo){
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file,tipo ,idnoticia:this.state.idnoticia}
    };
    fields.metodo = "/admin/houses/updateImg";
    axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onloadProgrammers(), 500);
          if(tipo==="previo"){
            this.setState({img1:file.base64});
            this.editarPre();
          }else{
            this.setState({img2:file.base64});
            this.editarPri();
          }
          
        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      });
  }
  updateImgPre() {
    if (this.state.filePre && this.state.filePre.base64) {
      this.updateImg(this.state.filePre,'previo');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  updateImgPri() {
    if (this.state.filePri.base64) {
      this.updateImg(this.state.filePri,'principal');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  onSubmitForm() {
    const { file1, file2, nombre, textoprevio, textoprincipal } = this.state;
    if (file1 !== "" && file2 !== "" && nombre !== "" && textoprevio !== "" && textoprincipal !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { file1, file2, nombre, textoprevio, textoprincipal} };
      dataSend.metodo = "/admin/houses/";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadProgrammers();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("newsForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", idprogrammer: "", textoprevio: "", textoprincipal: "" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  getFile1(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file1: { extencion, base64 } })
  }
  getImgPre(files) {
    this.setState({ imgPreName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePre: { extencion, base64 } })
  }
  getImgPri(files) {
    this.setState({ imgPriName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePri: { extencion, base64 } })
  }
  getFile2(files) {
    this.setState({ imgName2: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file2: { extencion, base64 } })
  }
  setValues(data) {
    if (data.name) {
      const { name, textoprevio, textoprincipal, img1, img2, id } = data;
      this.setState({ nombre:name, textoprevio, textoprincipal, img1, img2, idprogrammer: id });
      this.setState({ editNew: !this.state.editNew });
      this.toggleBox();
    } else {
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "", img1: "", img2: "" });
    }
  }
  onRemoveProgrammer(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteProgrammer(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  deleteProgrammer(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcasaprogramadora": id }
      };
      fields.metodo = "/admin/houses/delete";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onloadProgrammers(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onloadProgrammers(){
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/app/general/houses/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
    .then(res => {
      if (res.data.response) {
        console.log(res.data.response);
        const casasProgramadoras = res.data.response.map((prop) => {
            return {
              id: prop.idcasaprogramadora,
              name: prop.nombre,
              img1:prop.imgprevia,
              img2:prop.imgprincipal,
              textoprevio:prop.textoprevio,
              textoprincipal:prop.textoprincipal,
              reg: prop.reg,
              datepublic: prop.fechaPublicacion,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.casasProgramadoras.find(o => o.id === prop.idcasaprogramadora);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                  <Button
                    onClick={() => {
                      let obj = this.state.casasProgramadoras.find(o => o.id === prop.idcasaprogramadora);
                      this.onRemoveProgrammer(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                </div>
              )
            };            
          });
        this.setState({ casasProgramadoras });
      }
    })
  }
  componentDidMount() {
    this.onloadProgrammers();
  }
  render() {
    const {newProgrammer, editNew, preview, editarImgPre, editarImgPri} = this.state;
    return (
      <div className="main-content">
      {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
            {preview && (
              <Row>
                <Col md={12}>
                  <Card
                    content={
                      <BlockUi tag="div" blocking={this.state.blocking}>
                        <Row>
                          <Col md={12}>
                            <Col md={1} className="pr0 mt5" onClick={this.togglePreview}>
                              <img src={Arrow} alt="Flecha" />
                            </Col>
                            <Col md={11} className="pl0 ml0">
                              <h4 className="col-md-6 ml0 pl0 numbers text-danger media">Previsualizar casa programadora</h4>
                            </Col>
                          </Col>
                          <Col md={12}>
                            <h3 className="col-md-6 fwbold numbers media">{this.state.nombre}</h3>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={12}>
                            <Col md={7} className="mb-2">
                              <img src={this.state.file1.base64 || this.state.img2} className="w100p" alt="242x200" />
                            </Col>
                            <Col md={7} className="mb-2">
                              <p className="category text-justify font-italic">
                                {this.state.textoprincipal}
                              </p>
                            </Col>
                          </Col>
                          <Col md={11}>
                            <Col md={5} >
                              <Button bsStyle="default" bsSize="large" fill onClick={this.togglePreview}>
                                Continuar edición
                              </Button>
                            </Col>
                            {!editNew && (
                              <Col md={5}>
                                <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                  Publicar
                                </Button>
                              </Col>
                            )}

                            {editNew && (
                              <Col md={5}>
                                <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                  Publicar
                              </Button>
                              </Col>
                            )}
                          </Col>
                        </Row>
                      </BlockUi>
                    }
                  />
                </Col>
              </Row>
            )}
            {!newProgrammer && !preview && ( 
            <Row>
              <Col md={12}>
                <Card
                  content={
                    <form id="newsForm">
                      <BlockUi tag="div" blocking={this.state.blocking}>
                        <Row>
                          <Col md={10} className="mb-2">
                            {!editNew && (
                              <h4 className="col-md-6 ml0 numbers text-danger media">Crear casa programadora</h4>
                            )}
                            {editNew && (
                              <h4 className="col-md-6 ml0 numbers text-danger media">Editar casa programadora</h4>
                            )}
                          </Col>
                          <Col md={2} className="text-right">
                            <Button bsStyle="info" bsSize="sm" fill wd="true" onClick={this.togglePreview}>
                              Previsualizar
                            </Button>
                          </Col>
                        </Row>
                        <Row> 
                          <Col md={12}>
                            <Col md={6}>
                              <FormGroup>
                                <ControlLabel>Titulo/nombre de casa programadora</ControlLabel>
                                <FormControl placeholder="Escribe un nombre" type="text" value={this.state.nombre} onChange={(e) => this.setState({ nombre: e.target.value })}/>
                              </FormGroup>
                            </Col>
                            <Col md={6}>
                              <FormGroup>
                                <ControlLabel>Texto previo</ControlLabel>
                                <FormControl placeholder="Escribe un texto" type="text" value={this.state.textoprevio} onChange={(e) => this.setState({ textoprevio: e.target.value })}/>
                              </FormGroup>
                            </Col>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={12}>
                            {!editNew && (
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Imagen principal</ControlLabel>
                                  <div className="custom_file_upload">
                                    <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccioné una imagen" name="file_info" />
                                    <div className="file_upload">
                                      <FileBase64
                                        multiple={false}
                                        onDone={this.getFile1.bind(this)} />
                                    </div>
                                  </div>
                                </FormGroup>
                              </Col>
                            )}
                            {!editNew && (
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Imagen previsualización</ControlLabel>
                                  <div className="custom_file_upload">
                                    <input type="text" disabled="true" className="file" value={this.state.imgName2} placeholder="Seleccioné una imagen" name="file_info" />
                                    <div className="file_upload">
                                      <FileBase64
                                        multiple={false}
                                        onDone={this.getFile2.bind(this)} />
                                    </div>
                                  </div>
                                </FormGroup>
                              </Col>
                            )}
                          </Col>
                        </Row>
                        <Row>
                          <Col md={12} className="pl15">
                            <Col md={12}>
                              <FormGroup>
                                <ControlLabel>Texto principal</ControlLabel>
                                <FormControl
                                rows="5"
                                placeholder="Titular circular"
                                type="text"
                                value={this.state.textoprincipal} onChange={(e) => this.setState({ textoprincipal: e.target.value })}
                                componentClass="textarea"/>
                              </FormGroup>
                            </Col>
                          </Col>
                        </Row>
                        {editNew && (
                          <Row>
                            <Col md={8} className="mb-2">
                              <Col md={6}>
                                {!editarImgPre && (
                                <Thumbnail src={this.state.img1} alt="242x200">
                                  <h3 className="text-center">Imagen previsualización</h3>
                                  <p className="text-center">
                                    <Button
                                      onClick={this.editarPre}
                                      bsStyle="primary"
                                      simple><i className="fa fa-edit"></i></Button>
                                  </p>
                                </Thumbnail>
                                )}
                                {editarImgPre && (
                                  <div className="thumbnail">
                                    <h3 className="text-center">Imagen previsualización</h3>
                                    <div className="custom_file_upload mb-2">
                                      <input type="text" disabled="true" className="file" value={this.state.img1} placeholder="Seleccioné una imagen" name="file_info" />
                                      <div className="file_upload">
                                        <FileBase64
                                          multiple={false}
                                          onDone={this.getImgPre.bind(this)} />
                                      </div>

                                    </div>
                                    <p className="text-center">
                                      <Button
                                        onClick={this.editarPre}
                                        bsStyle="danger"
                                        simple><i className="fa fa-times fa-2x"></i></Button>
                                      <Button
                                        onClick={this.updateImgPre}
                                        bsStyle="success"
                                        simple><i className="fa fa-check fa-2x"></i></Button>
                                    </p>
                                  </div>
                                )}
                              </Col>
                              <Col md={6}>
                                {!editarImgPri && (
                                  <Thumbnail src={this.state.img2} alt="242x200">
                                    <h3 className="text-center">Imagen principal</h3>
                                    <p className="text-center">
                                      <Button
                                        onClick={this.editarPri}
                                        bsStyle="primary"
                                        simple><i className="fa fa-edit"></i></Button>
                                    </p>
                                  </Thumbnail>
                                )}
                                {editarImgPri && (
                                  <div className="thumbnail">
                                    <h3 className="text-center">Imagen principal</h3>
                                    <div className="custom_file_upload mb-2">
                                      <input type="text" disabled="true" className="file" value={this.state.imgPriName} placeholder="Seleccioné una imagen" name="file_info" />
                                      <div className="file_upload">
                                        <FileBase64
                                          multiple={false}
                                          onDone={this.getImgPri.bind(this)} />
                                      </div>

                                    </div>
                                    <p className="text-center">
                                      <Button
                                        onClick={this.editarPri}
                                        bsStyle="danger"
                                        simple><i className="fa fa-times fa-2x"></i></Button>
                                      <Button
                                        onClick={this.updateImgPri}
                                        bsStyle="success"
                                        simple><i className="fa fa-check fa-2x"></i></Button>
                                    </p>
                                  </div>
                                )}
                              </Col>
                            </Col>
                          </Row>)}
                        <Row>
                          <Col md={12}>
                            <Col md={5} >
                              <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                Cancelar
                              </Button>
                            </Col>
                            {!editNew && (
                              <Col md={5}>
                                <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                  Agregar
                                </Button>
                              </Col>
                            )}
                            {editNew && (
                              <Col md={5}>
                                <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                  Guardar
                               </Button>
                              </Col>
                            )}
                          </Col>
                        </Row>
                      </BlockUi>
                    </form>
                  }
                />
              </Col>
            </Row>)}
            {newProgrammer && (
            <Row>
              <Col md={12}>
                <Card
                  content={
                    <Row>
                      <Col md={12}>
                        <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                          Crear nueva
                        </Button>
                      </Col>
                      <Col md={12} xs={12}>                 
                        <ReactTable
                          data={this.state.casasProgramadoras}
                          filterable
                          columns={[
                            {
                                Header: "Titulo/nombre",
                                accessor: "name"
                            },
                            {
                                Header: "Tipo",
                                accessor: "reg",   
                            },
                            {
                                Header: "Creada",
                                accessor: "datepublic",
                            },
                            {
                              Header: "",
                              accessor: "actions",
                              sortable: false,
                              filterable: false
                            }
                          ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay informacion disponible'
                            pageText='Pagina'
                            ofText='de'
                            rowsText='filas'

                            // Accessibility Labels
                            pageJumpText='ir a la pagina'
                            rowsSelectorText='filas por pagina'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        </Col>
                      </Row>
                    }
                  />
                </Col>
              </Row>
            )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
