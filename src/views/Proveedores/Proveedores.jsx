import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      proveedores: [],
      newProveedor: true,
      nombre: "",
      telefono: "",
      correo: "",
      alert: null,
      responsable: ""
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }
  toggleBox() {
    // check if box is currently opened
    const { newProveedor } = this.state;
    this.setState({
      
      // toggle value of `isOpened`
      newProveedor: !newProveedor,
    });
  }

  hideAlert() {
    this.setState({
      alert: null
    });
  }
  onSubmitForm(e) {
    var dataSend = this.state;
    if (dataSend.nombre !== "" && dataSend.telefono !== "" && dataSend.correo !== null
      && dataSend.responsable !== "") {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "nombre": dataSend.nombre,
          "telefono": dataSend.telefono,
          "correo": dataSend.correo,
          "responsable": dataSend.responsable
        }
      };
      fields.metodo = "/admin/suppliers/";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("distForm").reset();
            this.setState({ nombre: "", telefono: "", correo: "", responsable: "" });
            this.onLoadSuppliers();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }

  onLoadSuppliers() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/app/general/suppliers/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response.proveedores) {
          const proveedores = res.data.response.proveedores.map((prop, key) => {
            return {
              id: key,
              name: prop.nombre,
              number: prop.telefono,
              email: prop.correo,
              responsable: prop.responsable,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ proveedores });
        }
      });
  }
  componentDidMount() {
    this.onLoadSuppliers();
  }
  render() {
    const { newProveedor } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {!newProveedor && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form>
                          <Col md={10} className="mb-2">
                            <h4 className="numbers text-danger media">Crear proveedor</h4>
                          </Col>

                          <BlockUi tag="div" blocking={this.state.blocking}>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Nombre de proveedor</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={(e) => { this.setState({ nombre: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Número de telefono</ControlLabel>
                                    <FormControl placeholder="Escribe un texto" value={this.state.telefono} onChange={(e) => { this.setState({ telefono: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Correo electrónico</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.correo} onChange={(e) => { this.setState({ correo: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Responsable de la región</ControlLabel>
                                    <FormControl placeholder="Escribe un texto" value={this.state.responsable} onChange={(e) => { this.setState({ responsable: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                          </Button>
                                </Col>
                                <Col md={5}>
                                  <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm.bind(this)}>
                                    Publicar
                            </Button>
                                </Col>
                              </Col>
                            </Row>

                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {newProveedor && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.proveedores}
                              filterable
                              columns={[
                                {
                                  Header: "Nombre proveedor",
                                  accessor: "name"
                                },
                                {
                                  Header: "Número contacto",
                                  accessor: "number"
                                },
                                {
                                  Header: "Correo electronico",
                                  accessor: "email",
                                },
                                {
                                  Header: "Responsable de la región",
                                  accessor: "responsable",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay informacion disponible'
                              pageText='Pagina'
                              ofText='de'
                              rowsText='filas'

                              // Accessibility Labels
                              pageJumpText='ir a la pagina'
                              rowsSelectorText='filas por pagina'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
