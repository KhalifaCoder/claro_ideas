import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, Thumbnail, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import FileBase64 from 'react-file-base64';
var moment = require('moment');
require('moment/locale/es');

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], newHerramienta: true ,file:{},imgName:"",textoprevio:"",textoprincipal:"",nombre:""};
    this.toggleBox = this.toggleBox.bind(this);
  }
  toggleBox() {
    // check if box is currently opened
    const { newHerramienta } = this.state;
    this.setState({
      // toggle value of `isOpened`
      newHerramienta: !newHerramienta,

    });
  }
  onLoadTools() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/app/general/tools/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        console.log(res.data.response);
        var fecha;
        if (res.data.response) {
          const Herramientas = res.data.response.map((prop, key) => {
            fecha = moment(prop.fechaPublicacion).format("DD/MM/YYYY");
            return {
              id: key,
              name: prop.nombre,
              fecha: fecha,
              image: (
                <Col md={5}>
                  <Thumbnail src={prop.img} alt="242x200">
                  </Thumbnail>
                </Col>
              ),
              actions: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.data.find(o => o.id === key);
                      alert(
                        "You've clicked LIKE button on \n{ \ntitulo: " +
                        obj.titulo +
                        ", \ntitulo: " +
                        obj.datepublic +
                        ", \ndatepublic: }"
                      );
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.data.find(o => o.id === key);
                      alert(
                        "You've clicked EDIT button on \n{ \ntitulo: " +
                        obj.titulo +
                        ", \ntitulo: " +
                        obj.datepublic +
                        ", \ndatepublic: }"
                      );
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-trash" />
                  </Button>
                </div>
              )
            };
          });
          this.setState({ Herramientas });
        }
      })
  }
  getFile(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file: { extencion, base64 } })
  }
  componentDidMount() {
    this.onLoadTools();
  }
  render() {
    const { newHerramienta } = this.state;
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              {!newHerramienta && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form>
                          <Row>
                            <Col md={12} className="mb-2">
                              <h4 className="numbers text-danger media">Crear herramienta comercial</h4>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={12} className="pl25">
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Nombre</ControlLabel>
                                  <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={(e)=>this.setState({nombre:e.target.value})} type="text" />
                                </FormGroup>
                              </Col>
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Texto previo</ControlLabel>
                                  <FormControl placeholder="Escribe un texto" value={this.state.textoprevio} onChange={(e)=>this.setState({textoprevio:e.target.value})} type="text" />
                                </FormGroup>
                              </Col>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={12} className="pl25">
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Imagen</ControlLabel>
                                  <div className="custom_file_upload">
                                    <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccioné una imagen" name="file_info" />
                                    <div className="file_upload">
                                      <FileBase64
                                        multiple={false}
                                        onDone={this.getFile.bind(this)} />
                                    </div>
                                  </div>
                                </FormGroup>
                              </Col>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={12} className="pl15">
                              <Col md={12}>
                                <FormGroup>
                                  <ControlLabel>Texto principal</ControlLabel>
                                  <FormControl
                                    rows="5"
                                    value={this.state.textoprincipal} onChange={(e)=>this.setState({textoprincipal:e.target.value})}
                                    placeholder="Escribe texto principal"
                                    type="text"
                                    componentClass="textarea" />
                                </FormGroup>
                              </Col>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={12}>
                              <Col md={5} >
                                <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                  Cancelar
                          </Button>
                              </Col>
                              <Col md={5}>
                                <Button bsStyle="danger" bsSize="large" fill>
                                  Agregar
                            </Button>
                              </Col>
                            </Col>
                          </Row>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {newHerramienta && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.Herramientas}
                              filterable
                              columns={[
                                {
                                  Header: "Imagen",
                                  accessor: "image"
                                },
                                {
                                  Header: "Titulo/nombre",
                                  accessor: "name",
                                },
                                {
                                  Header: "Fecha creación",
                                  accessor: "fecha",
                                },

                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay informacion disponible'
                              pageText='Pagina'
                              ofText='de'
                              rowsText='filas'

                              // Accessibility Labels
                              pageJumpText='ir a la pagina'
                              rowsSelectorText='filas por pagina'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
