import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], newCircular: true, titulo: "", texto: "", alert: null, idSelected: "", editCircular: false };
    this.toggleBox = this.toggleBox.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.deleteCirculars = this.deleteCirculars.bind(this);
    this.inactiveCirculars = this.inactiveCirculars.bind(this);

  }
  toggleBox() {
    const { newCircular } = this.state;
    if (newCircular === false) {
      this.setState({ titulo: "", texto: "" });
      this.setState({ editCircular: false });
    }
    this.setState({ newCircular: !newCircular });
  }

  hideAlert() {
    this.setState({
      alert: null
    });
  }
  onSubmitForm(e) {
    var dataSend = this.state;
    if (dataSend.texto !== "" && dataSend.titulo !== "") {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "titulo": dataSend.titulo,
          "texto": dataSend.texto
        }
      };
      fields.metodo = "/admin/circulars/";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }
  setValues(data) {
    if (data.texto) {
      const { titulo, texto, id } = data;
      this.setState({ titulo });
      this.setState({ texto });
      this.setState({ idSelected: id });
      this.setState({ editCircular: !this.state.editCircular });
      this.toggleBox();
    } else {
      this.setState({ titulo: "", texto: "", idSelected: "" });
    }
  }

  onUpdateCirculars() {
    const { titulo, texto, idSelected } = this.state;
    if (texto !== "" && titulo !== "" && idSelected !== "") {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { titulo, texto, "idcircular": idSelected }
      };
      fields.metodo = "/admin/circulars/update";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onRemoveCirculars(id) {
    this.setState({
      alert: (
        <SweetAlert
        style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={()=>this.deleteCirculars(id)}
          onCancel={()=>this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  onInactivateCirculars(id) {
    this.setState({
      alert: (
        <SweetAlert
        style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={()=>this.inactiveCirculars(id)}
          onCancel={()=>this.hideAlert()}
        >
          Esta accion inactivara esta novedad.
    </SweetAlert>
      )
    });
  }

  inactiveCirculars(id){
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {"idcircular": id }
      };
      fields.metodo = "/admin/circulars/inactive";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }

  deleteCirculars(id){
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {"idcircular": id }
      };
      fields.metodo = "/admin/circulars/delete";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onLoadCirculars() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/app/general/circulars/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response) {
          const circulares = res.data.response.map((prop, key) => {
            return {
              id: prop.idcircular,
              titulo: prop.titulo,
              texto: prop.texto,
              datepublic: prop.fechaPublicacion,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.circulares.find(o => o.id === prop.idcircular);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.circulares.find(o => o.id === prop.idcircular);
                      this.onInactivateCirculars(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-archive" />
                  </Button>
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.circulares.find(o => o.id === prop.idcircular);
                      this.onRemoveCirculars(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ circulares });
        }
      })
  }
  componentDidMount() {
    this.onLoadCirculars();
  }
  render() {
    const { newCircular, editCircular } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {!newCircular && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <BlockUi tag="div" blocking={this.state.blocking}>
                          <form>
                            {!editCircular && (
                              <Row>
                                <Col md={12} className="mb-2">
                                  <h4 className="numbers text-danger media">Crear circular</h4>
                                </Col>
                              </Row>)}
                            {editCircular && (
                              <Row>
                                <Col md={12} className="mb-2">
                                  <h4 className="numbers text-danger media">Editar circular</h4>
                                </Col>
                              </Row>)}

                            <Row>
                              <Col md={12} className="pl25">
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Titular circular</ControlLabel>
                                    <FormControl placeholder="Titular circular" value={this.state.titulo} onChange={(e) => { this.setState({ titulo: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12} className="pl15">
                                <Col md={12}>
                                  <FormGroup>
                                    <ControlLabel>Texto principal</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      placeholder="Titular circular"
                                      type="text"
                                      componentClass="textarea"
                                      value={this.state.texto} onChange={(e) => { this.setState({ texto: e.target.value }) }} />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                          </Button>
                                </Col>
                                {!editCircular && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm.bind(this)}>
                                      Agregar
                            </Button>
                                  </Col>)}
                                {editCircular && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onUpdateCirculars.bind(this)}>
                                      Guardar
                                  </Button>
                                  </Col>)}
                              </Col>
                            </Row>

                          </form>
                        </BlockUi>
                      }
                    />
                  </Col>
                </Row>)}
              {newCircular && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.circulares}
                              filterable
                              columns={[
                                {
                                  Header: "Titulo/nombre",
                                  accessor: "titulo"
                                },
                                {
                                  Header: "Creada",
                                  accessor: "datepublic",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay informacion disponible'
                              pageText='Pagina'
                              ofText='de'
                              rowsText='filas'

                              // Accessibility Labels
                              pageJumpText='ir a la pagina'
                              rowsSelectorText='filas por pagina'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
