import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import logo from "assets/img/logoIni.png";
import { Redirect } from "react-router-dom";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }
  redireccionar(){
    return(
    <Redirect from="/" to="/Dashboard"/>)
  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} mdOffset={4}>
            <img src={logo} className="w100p" alt="logo ideas"/>
          </Col>
                
              
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form>
              <Card
                hidden={this.state.cardHidden}
                className="pt0"
                shadow="ns"
                hCenter
                category="Ingresa tus datos de acceso"
                textCenter
                content={
                  <div>
                    <FormGroup>
                      <ControlLabel className="cgray ntt fs15">Correo electrónico</ControlLabel>
                      <FormControl className="login-input bsinput" placeholder="Escribe tu correo electrónico" type="email" />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="cgray ntt fs15">Contraseña</ControlLabel>
                      <FormControl className="login-input bsinput" placeholder="Ingresa tu contraseña" type="password" />
                    </FormGroup>
                    <FormGroup>
                      <p className="text-center fs13 fw200 cgray cpointer">¿Hás olvidado tu contraseña?</p>
                    </FormGroup>
                  </div>
                }
                legend={
                <div className="col-md-12 p0">
                  <Button bsStyle="danger" className="btn-fill text-center" bsSize="large" fill wd="true" onClick={this.redireccionar.bind(this)}>
                    Iniciar sesión
                  </Button>
                  </div>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default LoginPage;
