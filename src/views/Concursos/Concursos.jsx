import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: [], 
        concursos:[],
        newCompetition:true,
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.onImageChange = this.onImageChange.bind(this);
    this.onImageChange2 = this.onImageChange2.bind(this);
  }
  onImageChange(event) {
    var imgName = document.getElementById('file_upload').files[0].name;
    this.setState({ imgName });
  }
  onImageChange2(event) {
    var imgName2 = document.getElementById('file_upload_2').files[0].name;
    this.setState({ imgName2 });
  }
  toggleBox() {
    // check if box is currently opened
    const { newCompetition } = this.state;
    this.setState({
      // toggle value of `isOpened`
      newCompetition: !newCompetition,
    });
  }  

  componentDidMount(){
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/app/general/contests/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
    .then(res => {
      if (res.data.response) {
        const concursos = res.data.response.map((prop, key) => {
            return {
              id: key,
              title: prop.titulo,
              tipe: prop.tipo,
              datepublic: prop.fechaPublicacion,
              actions: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                        let obj = this.state.concursos.find(o => o.id === prop.idCategoria);
                        console.log(obj);
                    }}
                    
                    bsStyle="danger"
                    simple
                    // icon
                  >
                    <i className="fa fa-heart" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.data.find(o => o.id === key);
                      alert(
                        "You've clicked EDIT button on \n{ \ntitulo: " +
                        obj.titulo +
                        ", \ntitulo: " +
                        obj.datepublic +
                        ", \ndatepublic: }"
                      );
                    }}
                    bsStyle="warning"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-edit" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                  <Button
                    onClick={() => {
                      var data = this.state.data;
                      data.find((o, i) => {
                        if (o.id === key) {
                          // here you should add some custom code so you can delete the data
                          // from this component and from your server as well
                          data.splice(i, 1);
                          console.log(data);
                          return true;
                        }
                        return false;
                      });
                      this.setState({ data: data });
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-times" />
                  </Button>{" "}
                </div>
              )
            };            
          });
        this.setState({ concursos });
      }
    })
  }
  render() {
    const { newCompetition } = this.state;
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={12}>
            {!newCompetition && (
            <Row>
              <Col md={12}>
                <Card
                  content={
                    <form>
                      <Row>
                        <Col md={10} className="mb-2">
                          <h4 className="numbers text-danger media">Crear concurso</h4>
                        </Col>
                        <Col md={2} className="text-right">
                          <Button bsStyle="info" fill>
                            Previsualizar
                          </Button>
                        </Col>
                      </Row>
                      <Row> 
                        <Col md={12}>
                          <Col md={6}>
                            <FormGroup>
                              <ControlLabel>Titulo de concurso</ControlLabel>
                              <FormControl placeholder="Escribe un nombre" type="text" />
                            </FormGroup>
                          </Col>
                          <Col md={6}>
                            <FormGroup>
                              <ControlLabel>Texto previo</ControlLabel>
                              <FormControl placeholder="Escribe un texto" type="text" />
                            </FormGroup>
                          </Col>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={12}>
                          <Col md={6}>
                            <FormGroup>
                              <ControlLabel>Imagen principal</ControlLabel>
                              <div className="custom_file_upload">
                                <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccioné una imagen" name="file_info" />
                                <div className="file_upload">
                                  <input type="file" id="file_upload" onChange={this.onImageChange} name="file_upload" />
                                </div>
                              </div>
                            </FormGroup>
                          </Col>
                          <Col md={6}>
                            <FormGroup>
                              <ControlLabel>Imagen previsualización</ControlLabel>
                              <div className="custom_file_upload">
                                <input type="text" disabled="true" className="file" value={this.state.imgName2} placeholder="Seleccioné una imagen" name="file_info" />
                                <div className="file_upload">
                                  <input type="file" id="file_upload_2" onChange={this.onImageChange2} name="file_upload" />
                                </div>
                              </div>
                            </FormGroup>
                          </Col>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={12} className="pl15">
                          <Col md={12}>
                            <FormGroup>
                              <ControlLabel>Texto principal</ControlLabel>
                              <FormControl
                              rows="5"
                              placeholder="Titular circular"
                              type="text"
                              componentClass="textarea"/>
                            </FormGroup>
                          </Col>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={12}>
                          <Col md={5} >
                            <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                              Cancelar
                          </Button>
                          </Col>
                          <Col md={5}>
                            <Button bsStyle="danger" bsSize="large" fill>
                              Publicar
                            </Button>
                          </Col>
                        </Col>
                      </Row>
                    </form>
                  }
                />
              </Col>
            </Row>)}
            {newCompetition && (
            <Row>
              <Col md={12}>
                <Card
                  content={
                    <Row>
                      <Col md={12}>
                        <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                          Crear nueva
                          </Button>
                      </Col>
                      <Col md={12} xs={12}>                 
                        <ReactTable
                          data={this.state.concursos}
                          filterable
                          columns={[
                            {
                                Header: "Titulo/nombre",
                                accessor: "title"
                            },
                            {
                                Header: "Tipo",
                                accessor: "tipe",   
                            },
                            {
                                Header: "Creada",
                                accessor: "datepublic",
                            },
                            {
                              Header: "",
                              accessor: "actions",
                              sortable: false,
                              filterable: false
                            }
                          ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay informacion disponible'
                            pageText='Pagina'
                            ofText='de'
                            rowsText='filas'

                            // Accessibility Labels
                            pageJumpText='ir a la pagina'
                            rowsSelectorText='filas por pagina'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        </Col>
                      </Row>
                    }
                  />
                </Col>
              </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
