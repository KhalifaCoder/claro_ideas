import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup,Thumbnail, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import Select from "react-select";
import Datetime from "react-datetime";
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

import Arrow from "assets/img/left-arrow.png";
require('moment');
require('moment/locale/es');
class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      preview: false,
      editCam: false,
      imgPreName: "",
      filePre: {},
      imgPriName: "",
      filePri: {},
      editarImgPre: false,
      editarImgPri: false,
      imgName: "",
      link: "",
      categories:[],
      categoriaSelect: null,
      alert: null,
      imgName2: "",
      file1: {}, file2: {}, blocking: false, nombre: "", textoprevio: "", textoprincipal: "", img1: "", img2: "",
      campañas: [],
      idcampaña:"",
      newCampaña: false,
      regionSelect: null,
      tipos: [{ "label": "1", "value": "1" },
      { "label": "2", "value": "2" },
      { "label": "3", "value": "3" }]
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.togglePreview = this.togglePreview.bind(this);
    this.editarPre = this.editarPre.bind(this);
    this.updateImgPre = this.updateImgPre.bind(this);
    this.updateImgPri = this.updateImgPri.bind(this);
    this.editarPri = this.editarPri.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }
  toggleBox() {
    const { newCampaña ,editCam} = this.state;
    if (editCam=== true) {
      this.setState({ editCam: false });
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "", img1: "", img2: "" ,link:""});
      document.getElementById('fechaIni').value = "";
      document.getElementById('fechaFin').value = "";
    }
    this.setState({ newCampaña: !newCampaña });
  }
  editarPre() {
    this.setState({ editarImgPre: !this.state.editarImgPre })
  }
  editarPri() {
    this.setState({ editarImgPri: !this.state.editarImgPri })
  }
  togglePreview() {
    this.setState({ preview: !this.state.preview })
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  onLoadCategories(){
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/admin/campaigns/categories&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response) {
          const categories = res.data.response.map((prop) => {
            return {
              value: prop.idcategoria,
              label: prop.nombre
            };
          });
          this.setState({ categories });
        }
      })
  }
  
  onSubmitUpdateForm() {
    var fechaIni = document.getElementById('fechaIni').value;
    fechaIni = this.formatDate(fechaIni);
    fechaIni = Datetime.moment(fechaIni).format();
    var fechaFin = document.getElementById('fechaFin').value;
    fechaFin = this.formatDate(fechaFin);
    fechaFin = Datetime.moment(fechaFin).format();
    const { nombre, textoprevio, textoprincipal, singleSelect, idcampaña ,categoriaSelect,link} = this.state;
    if (nombre !== "" && textoprevio !== "" && textoprincipal !== "" && singleSelect != null && link!=="" && categoriaSelect!==null && fechaIni !== "" && fechaFin !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { nombre, textoprevio, textoprincipal, tipo: singleSelect.value, idcampaña,link,categoria:categoriaSelect.value ,fechaIni,fechaFin} };
      dataSend.metodo = "/admin/campaigns/update";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadCam();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("campaingsForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", textoprevio: "", textoprincipal: "", singleSelect: null,categoriaSelect:null,link:"" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  updateImg(file, tipo) {
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file, tipo, idcampaña: this.state.idcampaña }
    };
    fields.metodo = "/admin/campaigns/updateImg";
    axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onloadCam(), 500);
          if (tipo === "previo") {
            this.setState({ img1: file.base64 });
            this.editarPre();
          } else {
            this.setState({ img2: file.base64 });
            this.editarPri();
          }

        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      });
  }
  updateImgPre() {
    if (this.state.filePre && this.state.filePre.base64) {
      this.updateImg(this.state.filePre, 'previo');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  onInactivateCam(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.inactiveCam(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción archivará esta Campaña.
    </SweetAlert>
      )
    });
  }

  inactiveCam(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcampaña": id }
      };
      fields.metodo = "/admin/campaigns/inactive";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onloadCam();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }

  updateImgPri() {
    if (this.state.filePri.base64) {
      this.updateImg(this.state.filePri, 'principal');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  formatDate(date) {
    date = date.split("/");
    date = `${date[2]}-${date[1]}-${date[0]}`
    return date;
  }
  onSubmitForm() {
    var fechaIni = document.getElementById('fechaIni').value;
    fechaIni = this.formatDate(fechaIni);
    fechaIni = Datetime.moment(fechaIni).format();
    var fechaFin = document.getElementById('fechaFin').value;
    fechaFin = this.formatDate(fechaFin);
    fechaFin = Datetime.moment(fechaFin).format();
    const { file1, file2, nombre, textoprevio, textoprincipal, singleSelect, link, categoriaSelect } = this.state;
    if (file1 !== "" && file2 !== "" && nombre !== "" && textoprevio !== "" && textoprincipal !== "" && singleSelect !== null && link !== "" && categoriaSelect !== null && fechaIni !== "" && fechaFin !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { file1, file2, nombre, textoprevio, textoprincipal, tipo: singleSelect.value, link, cctegoria: categoriaSelect.value, fechaIni, fechaFin } };
      dataSend.metodo = "/admin/campaigns/";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadCam();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("campaingsForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", idnoticia: "", textoprevio: "", textoprincipal: "", singleSelect: null, link: "", categoriaSelect: null });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title=""
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }

  getFile1(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file1: { extencion, base64 } })
  }
  getImgPre(files) {
    this.setState({ imgPreName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePre: { extencion, base64 } })
  }

  getImgPri(files) {
    this.setState({ imgPriName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePri: { extencion, base64 } })
  }

  getFile2(files) {
    this.setState({ imgName2: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file2: { extencion, base64 } })
  }
  setValues(data) {
    if (data) {
      const { title, textoprevio, textoprincipal, img1, img2, id, link, dateStart, dateEnd } = data;
      this.setState({ nombre: title, textoprevio, textoprincipal, img1, img2, idcampaña: id, link });
      this.setState({ editCam: !this.state.editCam });
      this.toggleBox();
      setTimeout(function(){
      document.getElementById('fechaIni').value = dateStart;
      document.getElementById('fechaFin').value = dateEnd;
      },500);
    } 
  }

  onRemoveCampaing(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteCam(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }

  deleteCam(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcampaña": id }
      };
      fields.metodo = "/admin/campaigns/delete";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onloadCam(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onloadCam() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/app/general/campaigns/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response) {
          const campañas = res.data.response.map((prop, key) => {
            return {
              id: prop.idcampaña,
              idCategory: prop.idCategoria,
              title: prop.titulo,
              textoprevio: prop.textoprevio,
              textoprincipal: prop.textoprincipal,
              link: prop.link,
              datepublic: prop.fechaPublicacion,
              dateStart: prop.fechaInicio,
              dateEnd: prop.fechaFin,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.campañas.find(o => o.id === prop.idcampaña);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.campañas.find(o => o.id === prop.idcampaña);
                      this.onInactivateCam(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-archive" />
                  </Button>
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.campañas.find(o => o.id === prop.idcampaña);
                      this.onRemoveCampaing(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ campañas });
        }
      })
  }
  componentDidMount() {
    this.onloadCam();
    this.onLoadCategories();
  }
  render() {
    const { newCampaña, preview ,editCam,editarImgPre,editarImgPri} = this.state;
    var today = Datetime.moment().subtract(0, 'day');
    var valid = function (current) {
      return current.isAfter(today);
    };
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
            {preview && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <BlockUi tag="div" blocking={this.state.blocking}>
                          <Row>
                            <Col md={12}>
                              <Col md={1} className="pr0 mt5" onClick={this.togglePreview}>
                                <img src={Arrow} alt="Flecha" />
                              </Col>
                              <Col md={11} className="pl0 ml0">
                                <h4 className="col-md-6 ml0 pl0 numbers text-danger media">Previsualizar noticia</h4>
                              </Col>
                            </Col>
                            <Col md={12}>
                              <h3 className="col-md-6 fwbold numbers media">{this.state.nombre}</h3>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={12}>
                              <Col md={7} className="mb-2">
                                <img src={this.state.file1.base64 || this.state.img2} className="w100p" alt="242x200" />
                              </Col>
                              <Col md={7} className="mb-2">
                                <p className="category text-justify font-italic">
                                  {this.state.textoprincipal}
                                </p>
                              </Col>
                            </Col>
                            <Col md={11}>
                              <Col md={5} >
                                <Button bsStyle="default" bsSize="large" fill onClick={this.togglePreview}>
                                  Continuar edición
                                </Button>
                              </Col>
                              {!editCam && (
                                <Col md={5}>
                                  <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                    Publicar
                                </Button>
                                </Col>
                              )}

                              {editCam && (
                                <Col md={5}>
                                  <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                    Publicar
                                </Button>
                                </Col>
                              )}
                            </Col>
                          </Row>
                        </BlockUi>
                      }
                    />
                  </Col>
                </Row>
              )}
              {newCampaña && !preview && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form id="campaingsForm">
                          <BlockUi tag="div" blocking={this.state.blocking}>
                            <Row>
                              <Col md={10} className="mb-2">
                                {!editCam && (
                                  <h4 className="numbers pl15 text-danger media ml0">Crear campaña</h4>
                                )}
                                {editCam && (
                                  <h4 className="numbers pl15 text-danger media ml0">Editar campaña</h4>
                                )}
                              </Col>
                              <Col md={2} className="text-right">
                                <Button bsStyle="info" fill onClick={this.togglePreview}>
                                  Previsualizar
                          </Button>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Titulo de campaña</ControlLabel>
                                    <FormControl placeholder="Escribe un titulo" value={this.state.nombre} onChange={(e) => this.setState({ nombre: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <ControlLabel>Tipo de diagramación</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.tipos}
                                    onChange={(value) =>
                                      this.setState({ singleSelect: value })
                                    }
                                  />
                                </Col>

                              </Col>
                            </Row>
                            <Row>
                              <Col md={12} className="pl25">
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Categoria</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="sucursalSelect"
                                      value={this.state.categoriaSelect}
                                      options={this.state.categories}
                                      onChange={value =>
                                        this.setState({ categoriaSelect: value })
                                      } />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Texto previo</ControlLabel>
                                    <FormControl placeholder="Escribe un texto" value={this.state.textoprevio} onChange={(e) => this.setState({ textoprevio: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {!editCam && (
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Imagen principal</ControlLabel>
                                    <div className="custom_file_upload">
                                      <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccioné una imagen" name="file_info" />
                                      <div className="file_upload">
                                        <FileBase64
                                          multiple={false}
                                          onDone={this.getFile1.bind(this)} />
                                      </div>
                                    </div>
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Imagen previsualización</ControlLabel>
                                    <div className="custom_file_upload">
                                      <input type="text" disabled="true" className="file" value={this.state.imgName2} placeholder="Seleccioné una imagen" name="file_info" />
                                      <div className="file_upload">
                                        <FileBase64
                                          multiple={false}
                                          onDone={this.getFile2.bind(this)} />
                                      </div>
                                    </div>
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            )}
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Fecha inicial</ControlLabel>
                                    <Datetime
                                      locale="es"
                                      timeFormat={false}
                                      isValidDate={valid}
                                      inputProps={{ placeholder: "Escoge una fecha", id: "fechaIni" }}
                                    />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Fecha final</ControlLabel>
                                    <Datetime
                                      locale="es"
                                      timeFormat={false}
                                      isValidDate={valid}
                                      inputProps={{ placeholder: "Escoge una fecha", id: "fechaFin" }}
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12} className="pl15">
                                <Col md={12}>
                                  <FormGroup>
                                    <ControlLabel>Texto principal</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      value={this.state.textoprincipal}
                                      onChange={(e) => this.setState({ textoprincipal: e.target.value })}
                                      placeholder="Escribe el texto principal de la campaña"
                                      type="text"
                                      componentClass="textarea" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Link</ControlLabel>
                                    <FormControl placeholder="Escribe un link" value={this.state.link} onChange={(e) => this.setState({ link: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {editCam && (
                              <Row>
                                <Col md={8} className="mb-2">
                                  <Col md={6}>
                                    {!editarImgPre && (<Thumbnail src={this.state.img1} alt="242x200">
                                      <h3 className="text-center">Imagen previsualización</h3>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editarPre}
                                          bsStyle="primary"
                                          simple><i className="fa fa-edit"></i></Button>
                                      </p>
                                    </Thumbnail>
                                    )}
                                    {editarImgPre && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen previsualización</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgPreName} placeholder="Seleccioné una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImgPre.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPre}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImgPre}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                  <Col md={6}>
                                    {!editarImgPri && (
                                      <Thumbnail src={this.state.img2} alt="242x200">
                                        <h3 className="text-center">Imagen principal</h3>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPri}
                                            bsStyle="primary"
                                            simple><i className="fa fa-edit"></i></Button>
                                        </p>
                                      </Thumbnail>
                                    )}
                                    {editarImgPri && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen principal</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgPriName} placeholder="Seleccioné una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImgPri.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPri}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImgPri}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                </Col>
                              </Row>)}
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                                </Button>
                                </Col>
                                {!editCam && (
                                <Col md={5}>
                                  <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                    Publicar
                            </Button>
                                </Col>
                                )}
                                {editCam && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                      Publicar
                                </Button>
                                  </Col>
                                )}
                              </Col>
                            </Row>
                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {!newCampaña && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nueva
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.campañas}
                              filterable
                              columns={[
                                {
                                  Header: "Categoria",
                                  accessor: "idCategory"
                                },
                                {
                                  Header: "Titulo/nombre",
                                  accessor: "title"
                                },
                                {
                                  Header: "Fecha de publicación",
                                  accessor: "datepublic",
                                },
                                {
                                  Header: "Fecha de inicio",
                                  accessor: "dateStart",
                                },
                                {
                                  Header: "Fecha fin",
                                  accessor: "dateEnd",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay informacion disponible'
                              pageText='Pagina'
                              ofText='de'
                              rowsText='filas'

                              // Accessibility Labels
                              pageJumpText='ir a la pagina'
                              rowsSelectorText='filas por pagina'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
