import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, Nav, NavItem, Tab, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import Select from "react-select";
import Datetime from "react-datetime";
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import Checkbox from "components/CustomCheckbox/CustomCheckbox.jsx";

require('moment');
require('moment/locale/es');

class Merchandising extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enterprises: [],
      newEnterprice: false,
      persons: [],
      newProfile: false,
      nombre: "",
      blocking: false,
      apellidos: "",
      correo: "",
      fechaNacimiento: "",
      hobbies: "",
      documento: "",
      fijo: "",
      movil: "",
      alert: null,
      singleSelect: null,
      sucursalSelect: null,
      regionSelect: null,
      tipoDocSelect: null,
      imgName: "",
      files: [],
      nombreSucursal:"",
      ciudadSucursal:"",
      direccionSucursal:"",
      correoSucursal:"",
      telefonoSucursal:"",
      razon:"",
      nit:"",
      branchestemp: [],
      branches: [],
     tipos: [{ "label": "Cedula de ciudadania", "value": "1" }],
      regiones: [{ "label": "REGION 1", "value": "REGION 1" },
      { "label": "REGION 2", "value": "REGION 2" },
      { "label": "REGION 3", "value": "REGION 3" },
      { "label": "REGION 4", "value": "REGION 4" }]
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.toggleEnterprice = this.toggleEnterprice.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }
  toggleBox() {
    const { newProfile } = this.state;
    this.setState({
      newProfile: !newProfile
    });
  }
  onSubmitBranchesForm(){
    const {razon,nit,nombreSucursal,ciudadSucursal,direccionSucursal,correoSucursal,telefonoSucursal} = this.state;
    if(razon!=="" && nit!=="" && nombreSucursal!==""&& ciudadSucursal!=="" && direccionSucursal!=="" && correoSucursal!=="" && telefonoSucursal!==""){
      this.setState({ blocking: !this.state.blocking });
      var principal=document.getElementById("principal").value; 
      principal=(principal===true)?1:0;
      let dataSend={data:{razon,principal,nit,nombreSucursal,ciudadSucursal,direccionSucursal,correoSucursal,telefonoSucursal}};
      dataSend.metodo="/admin/enterprises/";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("branchesForm").reset();
            this.setState({ razon: "", nit: "", nombreSucursal: "", ciudadSucursal: "", direccionSucursal: "", telefonoSucursal: "", correoSucursal: ""});
            this.onLoadEnterprise();
            this.toggleEnterprice();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });
    }else{
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
          Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  toggleEnterprice() {
    this.setState({
      newEnterprice: !this.state.newEnterprice
    });
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  getFiles(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ files: { extencion, base64 } })
  }
  onSubmitForm(e) {
    var dataSend = this.state;
    dataSend.fechaNacimiento = document.getElementById('fechaNacimiento').value;
    if (dataSend.nombre !== "" && dataSend.apellidos !== "" && dataSend.tipoDocSelect !== null
      && dataSend.documento !== "" && dataSend.fechaNacimiento !== "" && dataSend.movil !== ""
      && dataSend.fijo !== "" && dataSend.correo !== "" && dataSend.singleSelect !== null
      && dataSend.sucursalSelect !== null && dataSend.regionSelect !== null) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "nombres": dataSend.nombre,
          "apellidos": dataSend.apellidos,
          "idTipoDocumento": dataSend.tipoDocSelect.value,
          "numeroDocumento": dataSend.documento,
          "fechaNacimiento": dataSend.fechaNacimiento,
          "movil": dataSend.movil,
          "fijo": dataSend.fijo,
          "foto": dataSend.files,
          "correo": dataSend.correo,
          "idEmpresa": dataSend.singleSelect.value,
          "idSucursal": dataSend.sucursalSelect.value,
          "hobbies": dataSend.hobbies,
          "region": dataSend.regionSelect.value
        }
      };
      fields.metodo = "/admin/dist/";
      axios.post("https://proxy-ideas-cuajahoo.c9users.io/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("distForm").reset();
            this.setState({ nombre: "", apellidos: "", tipoDocSelect: null, documento: "", movil: "", fijo: "", correo: "", singleSelect: null, sucursalSelect: null, imgName: "", regionSelect: null, hobbies: "" });
            this.onLoadDist();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }

  onInputNameChange(e) {
    this.setState({ nombre: e.target.value });
  }
  onInputLastNameChange(e) {
    this.setState({ apellidos: e.target.value });
  }
  onInputEmailChange(e) {
    this.setState({ correo: e.target.value });
  }
  onInputDateChange(e) {
    this.setState({ fechaNacimiento: e.target.value });
  }
  onInputDNChange(e) {
    this.setState({ documento: e.target.value });
  }
  onInputFijoChange(e) {
    this.setState({ fijo: e.target.value });
  }
  onInputMovilChange(e) {
    this.setState({ movil: e.target.value });
  }
  onInputHobbiesChange(e) {
    this.setState({ hobbies: e.target.value });
  }
  loadDataBranches(id) {
    let branches = [];
    this.state.branchestemp.map((prop, key) => {
      if (prop.id === id.id) {
        branches.push(prop)
      }
      return key;
    });
    this.setState({ singleSelect: id })
    this.setState({ branches });
  }
  onLoadDist() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/admin/dist/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response) {
          const persons = res.data.response.map((prop, key) => {
            return {
              id: key,
              nombres: prop.nombres,
              apellidos: prop.apellidos,
              correo: prop.correo,
              empresa: prop.nombreEmpresa,
              actions: (
                <div className="actions-right">
                  <Button
                    onClick={() => {
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-eye" />
                  </Button>
                </div>
              )
            };
          });
          this.setState({ persons });
        }
      });
  }
  onLoadEnterprise() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/admin/enterprises/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response) {
          const enterprises = res.data.response.map((prop, key) => {
            return {
              value: prop.idEmpresa,
              id: prop.idEmpresa,
              label: prop.nombre,
              nombre: prop.nombre,
              NIT: prop.NIT,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.enterprises.find(o => o.id === prop.idEmpresa);
                      console.log(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.data.find(o => o.id === key);
                      alert(
                        "You've clicked EDIT button on \n{ \nName: " +
                        obj.name +
                        ", \nposition: " +
                        obj.position +
                        ", \noffice: " +
                        obj.office +
                        ", \nage: " +
                        obj.age +
                        "\n}."
                      );
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ enterprises });
        }
      });
  }
  onLoadBranchs() {
    axios.get("https://proxy-ideas-cuajahoo.c9users.io/?url=/admin/branches/&token=U2FsdGVkX1%2BchsYtyMJEvB51mkO2SJShxpk87IpRCuHIoF%2FjtxKRDzXeJZaZKfQIxQBR5Phd%2BgF0lJ%2BDN6POpGSwjFtqb%2F7cdLh4mWiTmJgroNX4qDx1VtXfl6Uzlhq1oXKQn%2Bcxh0sHfd2n1wlFOeODI0cbOaXS02W8Gi1mnDwQwfVqziMcKRUY8i8lCdsK")
      .then(res => {
        if (res.data.response) {
          const branchestemp = res.data.response.map((prop, key) => {

            return {
              value: prop.idSucursal,
              id: prop.idEmpresa,
              label: prop.nombre
            };
          });
          this.setState({ branchestemp });
        }
      });
  }
  componentDidMount() {
    this.onLoadDist();
    this.onLoadEnterprise();
    this.onLoadBranchs();
  }
  render() {
    const { newProfile, newEnterprice } = this.state;
    var yesterday = Datetime.moment().subtract(1, 'day');
    var valid = function (current) {
      return !current.isAfter(yesterday);
    };
    const tabs = (
      <Tab.Container id="tabs-with-dropdown" defaultActiveKey="pedidos">
        <Row className="clearfix">
          <Col sm={12}>
            <Nav bsStyle="tabs">
              <NavItem eventKey="pedidos">Pedidos</NavItem>
              <NavItem eventKey="stock">Stock por distribuidor</NavItem>
              <NavItem eventKey="inventario">Inventario</NavItem>
            </Nav>
          </Col>
          <Col sm={12}>
            <Tab.Content animation>
              <Tab.Pane eventKey="pedidos">
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true">
                        Descargar CSV
                      </Button>
                    </Col>
                  </Row>
                )}
                {newProfile && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12}>
                        <h4 className="numbers text-danger">Crear distribuidor</h4>
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="distForm">
                              <Col md={12}>
                                <Col md={4} className="pl5">
                                  <FormGroup>
                                    <ControlLabel>Nombre (s)</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={this.onInputNameChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Apellido (s)</ControlLabel>
                                    <FormControl placeholder="Escribe un apellido" value={this.state.apellidos} onChange={this.onInputLastNameChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Tipo de documento</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="tipoDocSelect"
                                      value={this.state.tipoDocSelect}
                                      options={this.state.tipos}
                                      onChange={(value) =>
                                        this.setState({ tipoDocSelect: value })
                                      }
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Número de documento</ControlLabel>
                                    <FormControl placeholder="Escribe un numero de documento" value={this.state.documento} onChange={this.onInputDNChange.bind(this)} type="number" />
                                  </FormGroup>
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Fecha de nacimiento</ControlLabel>
                                    <Datetime
                                      locale="es"
                                      timeFormat={false}
                                      isValidDate={valid}
                                      inputProps={{ placeholder: "Escoge una fecha", id: "fechaNacimiento" }}
                                    />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Teléfono móvil</ControlLabel>
                                    <FormControl placeholder="Escribe numero movil" value={this.state.movil} onChange={this.onInputMovilChange.bind(this)} type="number" />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Teléfono fijo</ControlLabel>
                                    <FormControl placeholder="Escribe numero fijo" value={this.state.fijo} onChange={this.onInputFijoChange.bind(this)} type="number" />
                                  </FormGroup>
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Correo electrónico</ControlLabel>
                                    <FormControl placeholder="Escribe un correo" value={this.state.correo} onChange={this.onInputEmailChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <ControlLabel>Empresa</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.enterprises}
                                    onChange={(idEmpresa) =>
                                      this.loadDataBranches(idEmpresa)
                                    }
                                  />
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <ControlLabel>Sucursal</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="sucursalSelect"
                                    value={this.state.sucursalSelect}
                                    options={this.state.branches}
                                    onChange={value =>
                                      this.setState({ sucursalSelect: value })
                                    }
                                  />
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Foto</ControlLabel>
                                    <div className="custom_file_upload">
                                      <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccioné una imagen" name="file_info" />
                                      <div className="file_upload">
                                        <FileBase64
                                          multiple={false}
                                          onDone={this.getFiles.bind(this)} />
                                      </div>
                                    </div>
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <ControlLabel>Region</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="sucursalSelect"
                                    value={this.state.regionSelect}
                                    options={this.state.regiones}
                                    onChange={value =>
                                      this.setState({ regionSelect: value })
                                    }
                                  />
                                </Col>

                              </Col>
                              <Col md={12} className="mb-2">
                                <Col md={12} >
                                  <FormGroup>
                                    <ControlLabel>Hobbies</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      value={this.state.hobbies} onChange={this.onInputHobbiesChange.bind(this)}
                                      componentClass="textarea"
                                      bsClass="form-control rsn"
                                      placeholder="Escriba hobbies"
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                            </Button>
                                </Col>
                                <Col md={5}>
                                  <Button bsStyle="danger" type="button" onClick={this.onSubmitForm.bind(this)} bsSize="large" fill >
                                    Agregar
                            </Button>
                                </Col>
                              </Col>
                            </form>
                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.persons}
                            filterable
                            columns={[
                              {
                                Header: "Empresa",
                                accessor: "empresa"
                              },
                              {
                                Header: "Nombres",
                                accessor: "nombres"
                              },
                              {
                                Header: "Apellidos",
                                accessor: "apellidos"
                              },
                              {
                                Header: "Correo",
                                accessor: "correo"
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay informacion disponible'
                            pageText='Pagina'
                            ofText='de'
                            rowsText='filas'

                            // Accessibility Labels
                            pageJumpText='ir a la pagina'
                            rowsSelectorText='filas por pagina'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>
                )}
              </Tab.Pane>
              <Tab.Pane eventKey="stock">
                {!newEnterprice && (
                  <Row>
                    <Col md={12}>
                      <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleEnterprice}>
                        Crear nueva
                      </Button>
                    </Col>
                  </Row>)}
                {newEnterprice && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12}>
                        <h4 className="numbers text-danger">Crear empresa</h4>
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="branchesForm">
                              <Col md={12}>
                                <Col md={4} className="pl5">
                                  <FormGroup>
                                    <ControlLabel>Nombre /razón social</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre o razón social" value={this.state.razon} onChange={(e)=>{this.setState({razon:e.target.value})}} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>NIT</ControlLabel>
                                    <FormControl placeholder="Escribe un NIT" value={this.state.nit} onChange={(e)=>{this.setState({nit:e.target.value})}} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <h4 className="mtext numbers text-danger">Sucursal</h4>
                              </Col>
                              <Col md={12} >
                                <Col md={12} className="pl5">
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Nombre</ControlLabel>
                                      <FormControl id="nombre" placeholder="Escribe un nombre" value={this.state.nombreSucursal} onChange={(e)=>{this.setState({nombreSucursal:e.target.value})}} type="text" />
                                    </FormGroup>
                                  </Col>

                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Ciudad</ControlLabel>
                                      <FormControl id="ciudad" placeholder="Escribe una ciudad" value={this.state.ciudadSucursal} onChange={(e)=>{this.setState({ciudadSucursal:e.target.value})}} type="text" />
                                    </FormGroup>
                                  </Col>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Dirección</ControlLabel>
                                      <FormControl id="direccion" placeholder="Escribe una dirección" value={this.state.direccionSucursal} onChange={(e)=>{this.setState({direccionSucursal:e.target.value})}} type="text" />
                                    </FormGroup>
                                  </Col>
                                </Col>
                                <Col md={12}>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Teléfono</ControlLabel>
                                      <FormControl id="telefono" placeholder="Escribe un numero de telefono" value={this.state.telefonoSucursal} onChange={(e)=>{this.setState({telefonoSucursal:e.target.value})}} type="number" />
                                    </FormGroup>
                                  </Col>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Correo electrónico</ControlLabel>
                                      <FormControl id="correo" placeholder="Escribe un correo" value={this.state.correoSucursal} onChange={(e)=>{this.setState({correoSucursal:e.target.value})}} type="text" />
                                    </FormGroup>
                                  </Col>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Sucursal principal</ControlLabel>
                                      <Checkbox
                                        number="principal"
                                      />
                                    </FormGroup>
                                  </Col>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleEnterprice}>
                                    Cancelar
                                  </Button>
                                </Col>
                                <Col md={5}>
                                  <Button bsStyle="danger" type="button" onClick={this.onSubmitBranchesForm.bind(this)} bsSize="large" fill >
                                    Agregar
                                  </Button>
                                </Col>
                              </Col>
                            </form>
                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newEnterprice && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.enterprises}
                            filterable
                            columns={[
                              {
                                Header: "Nombre",
                                accessor: "nombre"
                              },
                              {
                                Header: "NIT",
                                accessor: "NIT"
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay informacion disponible'
                            pageText='Pagina'
                            ofText='de'
                            rowsText='filas'

                            // Accessibility Labels
                            pageJumpText='ir a la pagina'
                            rowsSelectorText='filas por pagina'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>)}
              </Tab.Pane>
              <Tab.Pane eventKey="inventario">
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                        Crear nuevo
                      </Button>
                    </Col>
                  </Row>
                )}
                {newProfile && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12}>
                        <h4 className="numbers text-danger">Crear distribuidor</h4>
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="distForm">
                              <Col md={12}>
                                <Col md={4} className="pl5">
                                  <FormGroup>
                                    <ControlLabel>Nombre (s)</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={this.onInputNameChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Apellido (s)</ControlLabel>
                                    <FormControl placeholder="Escribe un apellido" value={this.state.apellidos} onChange={this.onInputLastNameChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Tipo de documento</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="tipoDocSelect"
                                      value={this.state.tipoDocSelect}
                                      options={this.state.tipos}
                                      onChange={(value) =>
                                        this.setState({ tipoDocSelect: value })
                                      }
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Número de documento</ControlLabel>
                                    <FormControl placeholder="Escribe un numero de documento" value={this.state.documento} onChange={this.onInputDNChange.bind(this)} type="number" />
                                  </FormGroup>
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Fecha de nacimiento</ControlLabel>
                                    <Datetime
                                      locale="es"
                                      timeFormat={false}
                                      isValidDate={valid}
                                      inputProps={{ placeholder: "Escoge una fecha", id: "fechaNacimiento" }}
                                    />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Teléfono móvil</ControlLabel>
                                    <FormControl placeholder="Escribe numero movil" value={this.state.movil} onChange={this.onInputMovilChange.bind(this)} type="number" />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Teléfono fijo</ControlLabel>
                                    <FormControl placeholder="Escribe numero fijo" value={this.state.fijo} onChange={this.onInputFijoChange.bind(this)} type="number" />
                                  </FormGroup>
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Correo electrónico</ControlLabel>
                                    <FormControl placeholder="Escribe un correo" value={this.state.correo} onChange={this.onInputEmailChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <ControlLabel>Empresa</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.enterprises}
                                    onChange={(idEmpresa) =>
                                      this.loadDataBranches(idEmpresa)
                                    }
                                  />
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <ControlLabel>Sucursal</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="sucursalSelect"
                                    value={this.state.sucursalSelect}
                                    options={this.state.branches}
                                    onChange={value =>
                                      this.setState({ sucursalSelect: value })
                                    }
                                  />
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Foto</ControlLabel>
                                    <div className="custom_file_upload">
                                      <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccioné una imagen" name="file_info" />
                                      <div className="file_upload">
                                        <FileBase64
                                          multiple={false}
                                          onDone={this.getFiles.bind(this)} />
                                      </div>
                                    </div>
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <ControlLabel>Region</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="sucursalSelect"
                                    value={this.state.regionSelect}
                                    options={this.state.regiones}
                                    onChange={value =>
                                      this.setState({ regionSelect: value })
                                    }
                                  />
                                </Col>

                              </Col>
                              <Col md={12} className="mb-2">
                                <Col md={12} >
                                  <FormGroup>
                                    <ControlLabel>Hobbies</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      value={this.state.hobbies} onChange={this.onInputHobbiesChange.bind(this)}
                                      componentClass="textarea"
                                      bsClass="form-control rsn"
                                      placeholder="Escriba hobbies"
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                            </Button>
                                </Col>
                                <Col md={5}>
                                  <Button bsStyle="danger" type="button" onClick={this.onSubmitForm.bind(this)} bsSize="large" fill >
                                    Agregar
                            </Button>
                                </Col>
                              </Col>
                            </form>
                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.persons}
                            filterable
                            columns={[
                              {
                                Header: "Empresa",
                                accessor: "empresa"
                              },
                              {
                                Header: "Nombres",
                                accessor: "nombres"
                              },
                              {
                                Header: "Apellidos",
                                accessor: "apellidos"
                              },
                              {
                                Header: "Correo",
                                accessor: "correo"
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay informacion disponible'
                            pageText='Pagina'
                            ofText='de'
                            rowsText='filas'

                            // Accessibility Labels
                            pageJumpText='ir a la pagina'
                            rowsSelectorText='filas por pagina'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>
                )}
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    );
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card content={tabs} />
            </Col>
          </Row>

        </Grid>
      </div>
    );
  }
}

export default Merchandising;
