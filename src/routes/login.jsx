import LoginPage from "views/login/LoginPage";

const LoginRoutes = [
  {
    path: "/",
    name:"Login",
    component: LoginPage
  }
];

export default LoginRoutes;
