import Dashboard from "views/Dashboard/Dashboard";
//import UserProfile from "views/UserProfile/UserProfile";
import Notifications from "views/Notifications/Notifications";
import perfiles from "views/perfiles/perfiles";
import fabricantes from "views/fabricantes/fabricantes";
import novedades from "views/novedades/novedades";
import Campañas from "views/Campañas/Campañas";
import Noticias from "views/noticias/Noticias";
import Concursos from "views/Concursos/Concursos";
import Casas_programadoras from "views/CasasProgramadoras/CasasProgramadoras";
import Proveedores from "views/Proveedores/Proveedores";
import Herramientas from "views/Herramientas/Herramientas";
import Merchandising from "views/merchandising/merchandising";

/*import Icons from "views/Icons/Icons";
import Maps from "views/Maps/Maps";
import Upgrade from "views/Upgrade/Upgrade";*/

const dashboardRoutes = [
  {
    path: "/Banners",
    icon: "pe-7s-angle-right",
    name: "Banners",
    component: Dashboard
  },
  {
    path: "/perfiles",
    icon: "pe-7s-angle-right",
    name: "Acceso de perfiles",
    component: perfiles
  },
  {
    path: "/Noticias",
    icon: "pe-7s-angle-right",
    name: "Noticias",
    component: Noticias
  },
  {
    path: "/Campañas",
    icon: "pe-7s-angle-right",
    name: "Campañas",
    component: Campañas
  },
  {
    path: "/Herramientas",
    icon: "pe-7s-angle-right",
    name: "Herramientas comerciales",
    component: Herramientas
  },
  {
    path: "/Merchandising",
    icon: "pe-7s-angle-right",
    name: "Merchandising",
    component: Merchandising
  },
  {
    path: "/Concursos",
    icon: "pe-7s-angle-right",
    name: "Concursos",
    component: Concursos
  },
  {
    path: "/programadoras",
    icon: "pe-7s-angle-right",
    name: "Casas programadoras",
    component: Casas_programadoras
  },
  {
    path: "/fabricantes",
    icon: "pe-7s-angle-right",
    name: "fabricantes",
    component: fabricantes
  },
  {
    path: "/novedades",
    icon: "pe-7s-angle-right",
    name: "novedades",
    component: novedades
  },
  {
    path: "/proveedores",
    icon: "pe-7s-angle-right",
    name: "Directorio de proveedores",
    component: Proveedores
  },
  {
    path: "/Cerrar",
    icon: "pe-7s-angle-right",
    name: "Cerrar sesión",
    component: Notifications
  },
  { redirect: true, path: "/", to: "/Banners", name: "Banners" }
];

export default dashboardRoutes;
