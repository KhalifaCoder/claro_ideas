import Dashboard from "layouts/Dashboard/Dashboard.jsx";
import Pages from "layouts/Login/Login.jsx";

var indexRoutes = [{ path: "/", name: "Home", component: Dashboard },{ path: "/login", name: "Login", component: Pages }];

export default indexRoutes;
